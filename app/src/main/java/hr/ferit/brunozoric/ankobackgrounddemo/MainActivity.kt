package hr.ferit.brunozoric.ankobackgrounddemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {

    private val repeatCount = 5
    private val finishedMessage = "Glorious ANKO finished."
    private val started = "Started"
    private val working = "Working"
    private val finished = "Finished"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        startWorkAction.setOnClickListener{ runHeavyWorkload() }
    }

    fun runHeavyWorkload(){
        startWorkAction.text = started
        doAsync {
            for (i in 1..repeatCount){
                Thread.sleep(1000)
//                uiThread {
//                    messageDisplay.text = i.toString()
//                }
            }
            uiThread{
                messageDisplay.text = finishedMessage
                startWorkAction.text = finished
            }
        }
        startWorkAction.text = working
    }
}

